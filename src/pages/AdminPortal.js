import { useState, useEffect, useContext } from 'react';
import { Navigate } from "react-router-dom";
import { Form, Button, Card, Container, Row } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Link } from 'react-router-dom'


export default function AdminPortal() {

	const { user } = useContext(UserContext);


	return(

		<>

		<Card className="mt-5 mb-3 p-3">
			<Card.Body>
				<Card.Title className="text-center">Welcome Back Admin!</Card.Title>
			</Card.Body>
		</Card>


				
		<Row className="container-fluid">

		<Card className="col-lg-5 m-3">
			<Card.Body>
				<h3>See Inventory?</h3>
				
				<Button as={Link} to={'/products/edit' } className="p-3" variant="primary">Edit Inventory</Button>
			</Card.Body>
		</Card>
		</Row>

		<Row className="container-fluid">
		<Card className="m-3 col-lg-5">
			<Card.Body>
				<h3>See all Users?</h3>
				<Button as={Link} to={'/products/add' } className="p-3 disabled" variant="primary">All Users</Button>
			</Card.Body>
		</Card>

		<Card className="m-3 col-lg-5">
			<Card.Body>
				<h3>See Sales</h3>
				<Button as={Link} to={'/products' } className="p-3 m-2 disabled" variant="primary">Sales Records</Button>
				
			</Card.Body>
		</Card>
		</Row>




		</>


		)

}