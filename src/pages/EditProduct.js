import { useState, useEffect, useContext } from "react";
import { useNavigate, useParams} from "react-router-dom";
import {Form, Button, Card, Row, Col} from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";

export default function EditProduct(){


	const { productId } = useParams();

	const navigate = useNavigate();

	const [Nname, setName] = useState('');
	const [Ndescription, setDescription] = useState('');
	const [Nprice, setPrice] = useState(0);
	const [Nquantity,setQuantity] = useState(0);
	const [Nimagesrc, setSource] = useState('');

	useEffect(()=>{

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {


			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
			setSource(data.imagesrc);

		});

	}, [productId])


	const update = (e) => {

		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/update`,{
			method: "PATCH",
			headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
	        body: JSON.stringify({
	           	name: Nname,
	           	description: Ndescription,
	           	price: Nprice,
	           	quantity: Nquantity,
	           	imagesrc: Nimagesrc
	        })
		})
		.then(res => res.json())
		.then( async data => {
			 if(data){
                Swal.fire({
                    title: "PRODUCT UPDATE SUCCESS!",
                    icon: "success"
                }).then((res) => {
                if(res.isConfirmed){
                window.location.reload();

            }
                })

                setName('');
				setDescription('');
				setPrice('');
				setQuantity('');
				setSource('');
                navigate("/products/edit");
            }else{
                Swal.fire({
                    title: "Update Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
		})

	}


	return (
		<>
		<Form onSubmit={e => update(e)}>
		<Card className="m-3 p-5">
			
		<Row>
			
			<Col className="col-lg-6 col-sm-12">
				<Form.Group>
				<Card.Img src={Nimagesrc} style={{width: "100%", height: "100%"}} className="m-3 container-fluid"></Card.Img>
				<Card.Subtitle>Image URL here:</Card.Subtitle>
				<Form.Control
					onChange={e => setSource(e.target.value)}
                    value= {Nimagesrc}
                  	/>

                </Form.Group>

			</Col>

			<Col className="col-lg-6 col-sm-12">
				<Form.Group>
				<Card.Title className="m-3">
					Product Name
					<Form.Control
					className="m-1"
					onChange={e => setName(e.target.value)}
                    value= {Nname}
                  	/>
                </Card.Title>
                </Form.Group>

                <Form.Group>
                <Card.Subtitle className="m-3">
                    Description:
                    <Form.Control
                    className="m-1"
                	onChange={e => setDescription(e.target.value)}
                    value={Ndescription}
                	/>
                </Card.Subtitle>
                </Form.Group>
                	
               <Form.Group>
                <Card.Subtitle className="m-3">
                    Price:
                    <Form.Control
                    className="m-1"
                	onChange={e => setPrice(e.target.value)}
                    value={Nprice}
                	/>
                </Card.Subtitle>
                </Form.Group>


                <Form.Group>	
                <Card.Subtitle className="m-3">
                    Quantity
                    <Form.Control
                    className="m-1"
               		onChange={e => setQuantity(e.target.value)}
                    value={Nquantity}
                	/>
                </Card.Subtitle>
                </Form.Group>



			</Col>
		</Row>
		<Button variant="primary" type="submit" id="submitBtn" className="m-3">Submit</Button>
		</Card>

		

		</Form>
		</>

		)
}