import { useEffect, useState } from "react";
import { Card, Row, Col, Button, Container, Tab, Tabs } from 'react-bootstrap'
import AProductCard from "../components/AProductsCard";
import { Link } from "react-router-dom";

export default function Inventory(){


  const [inaProducts, setInactive] = useState([]);
  const [allProducts, setAllProducts]= useState([]);


  const [productId, setProductId] = useState("");
  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQty] = useState(0);
  const [imagesrc, setImgSource] = useState("");


  const AllProducts = () => {
  
           fetch(`${process.env.REACT_APP_API_URL}/products/all`,
        {
            method : "GET",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setAllProducts(data.map(product =>{
        return(
          <AProductCard key={product._id} prop={product}/>
        );
      }));
    })
  }
  

  useEffect(() =>{
    AllProducts();
    },[] );

  const AProducts = () => {
  
    fetch(`${process.env.REACT_APP_API_URL}/products/inactive`,
              {
            method : "GET",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      setInactive(data.map(product =>{
        return(
          <AProductCard key={product._id} prop={product}/>
        );
      }));
    })
  }
  

  useEffect(() =>{
    AProducts();
    },[] );
  
  const archived = () => {}
  



  return(
    <>

      <Tabs
        id="uncontrolled-tab-example"
        className="mt-3 bg-white"
      >

        <Tab eventKey="all" title="All" data-toggle="tab" href="#secA" className="container-fluid">
        <Button as={Link} to={'/products/add' } size="sm" className="mt-3" variant="info">Add New Product</Button>
            <Row id="secA">
              {allProducts}
           </Row>
        </Tab>

        <Tab eventKey="inactive" title="Inactive" data-toggle="tab" href="#secC" >

           <Row id="secC">
              {inaProducts}
           </Row>

        </Tab>


      </Tabs>

    </>
  )

}