import { useEffect, useState } from "react";
import { Card, Row, Col, Button } from 'react-bootstrap'
import ProductsCard from "../components/ProductsCard";




export default function Products() {


	const [products, setProducts] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductsCard key={product._id} productProp={product}/>
				);
			}));
		})
	},

	[] );


	return(
		<>	
			<h1 id="page-header" className="my-5">Products</h1>

			<Row >
			
				{products}
			
			</Row>

		</>
	)
}