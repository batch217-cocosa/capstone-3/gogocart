import { useContext, useState, useEffect } from "react"
import { Card, Button, Row, Col } from 'react-bootstrap';
import { Link, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import CartItems from "../components/CartCard"




export default function Cart() {

  const navigate = useNavigate();
	const {user} = useContext(UserContext);
	const [cartProduct, setProduct]= useState([]);
  const [checkOrderout, setCheckout] = useState(false);

	const [total, setTotal]= useState(0);

  const checkout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`,
        {
          method : "POST",
          headers : {
            "Content-Type" : "application/json",
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
      .then(data => {

        if(data){
          Swal.fire({
                    title: "ORDER SUCCESSFUL!",
                    icon: "success",
                    text: `Check order in User Inventory`
                })
          setCheckout(true);
          navigate("/products")

        } else {

          Swal.fire({
                    title: "Order Failed",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })

        }
      })
    }
 

  useEffect(()=>{
	fetch(`${process.env.REACT_APP_API_URL}/orders/cart`,
        {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
    .then(res => res.json())
    .then(data => {
      setTotal(data.totalAmount);
      setProduct(data.orderedProducts.map(product =>{
        return(
          <CartItems key={product._id} CardProp={product}/>
        );
      }));
    })
  }, [])
    return (
    	<>	
        {cartProduct}


      {

        (total != 0)
        ?
        <Row className="my-3">
        <Col className="my-3">
         
        </Col>

        <Col className="my-3">
           <Card className="my-3">
            <Card.Subtitle className="my-3 text-center"  >
            Total: {total}
            </Card.Subtitle>
          </Card>

          <Card className="bg-success my-3">
            <Button variant="success" className="p-3" onClick={() => checkout(user.id)}>Checkout</Button>
          </Card>
        </Col>
      </Row>
      :
      <Card className="p-5 m-5">
      <Card.Title>
      Your Cart is Empty
      </Card.Title>
      </Card>
      }
			
				
			
      
		  </>
    )


}