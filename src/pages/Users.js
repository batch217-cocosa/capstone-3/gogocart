import { useState, useEffect, useContext } from 'react';
import { Navigate } from "react-router-dom";
import { Form, Button, Card, Container, Row } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Link } from 'react-router-dom';
import AfterCo from '../components/AfterCo';
export default function UserPage(){

	const {user} = useContext(UserContext);

	const [fName, setFirst]=useState('');
	const [lName, setLast]=useState('');
	const [mobile, setMobile]=useState('');
	const [email, setEmail]=useState('');
	const [type, setType]=useState('');
	const [cart, setCart]=useState('');
	const [ongoing, setOngoing]=useState([]);
	const [complete, setComplete]=useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        method: "GET",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": `Bearer ${localStorage.getItem("token")}`
        }
    	})
    	.then(res => res.json())
    	.then(data => {
    		setFirst(data.firstName);
    		setLast(data.lastName);
    		setMobile(data.mobileNo);
    		setEmail(data.email);
    		setType(data.isAdmin);
    		setCart(data.openCart);
    		

    	})

	}, [])

	return(
		<>
		<Card className="m-3">
		<Card.Title className="m-3">
		<h1>Welcome {fName}!</h1>
		</Card.Title>

		<Card.Body>
		<Card.Subtitle>
		<h2 className="my-3">User Details</h2>
		</Card.Subtitle>
		<p>Name : {fName} {lName}</p>
		
		<p>mobile number : {mobile}</p>

		<p>email: {email}</p>

		<p>
		type: 
		{
			(type)
			?
			"Admin"
			:
			"Normal User"
		}</p>

		</Card.Body>
		</Card>

		<Card className="m-3" bg="info" >
		<Card.Title>
		<h3 className="m-3" >Ongoing Orders</h3>
		</Card.Title>
		<Card.Body>
		{ongoing}
		</Card.Body>
		</Card>

		<Card className="m-3" bg="success">
		<Card.Title>
		<h3 className="m-3">Completed Orders</h3>
		</Card.Title>
		<Card.Body>
		{complete}
		</Card.Body>
		</Card>
		
		</>
	)
}