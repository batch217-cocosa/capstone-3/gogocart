import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import {Form, Button, Card, Row} from "react-bootstrap";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { Link } from "react-router-dom";

export default function AddProduct(){

    const navigate = useNavigate();

    const [productName, setProduct] = useState('');
    const [description, setDescription] = useState('');
    const [productPrice, setPrice] = useState('');

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if(productName !== '' && description !== '' && productPrice !== ''){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    },[productName, description, productPrice])


function addNewProduct(e){

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/add`,{
        method: "POST",
        headers:{
            "Content-Type" : "application/json",
            "Authorization": `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({
            name: productName,
            description: description,
            price: productPrice
        })
    }) .then(async res => res.json()).then( data => {
        if(data){
            Swal.fire({
                title: "Product Added",
                icon: "success",
                text: "Check Inventory"
            });
            setProduct('');
            setDescription('');
            setPrice('');
            navigate("/products")
        } else {
            Swal.fire({
                title: "Error",
                icon: "error",
                text: "Check details"
            })
        }
    })

}

return (
    <>
    <Card className="my-5">
    <h1 className="mt-5 text-center">Add New Product</h1>

    <Form onSubmit={e => addNewProduct(e)}>
        <Card.Body>
    <Form.Group className="p-3" controlId="productName">
        <Form.Label>Product name</Form.Label>
        <Form.Control 
            type="text"
            placeholder="Enter Product name"
            value={productName}
            onChange={e => setProduct(e.target.value)}
            required
        />

    </Form.Group>
    </Card.Body>


     <Card.Body>
    <Form.Group className="p-3" controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control 
            type="text"
            placeholder="Enter description"
            value={description}
            onChange={e => setDescription(e.target.value)}
            required
        />

    </Form.Group>
     </Card.Body>


      <Card.Body>
    <Form.Group className="p-3" controlId="productPrice">
        <Form.Label>Price</Form.Label>
        <Form.Control 
            type="number"
            placeholder="Enter price"
            value={productPrice}
            onChange={e => setPrice(e.target.value)}
            required
        />

    </Form.Group>
     </Card.Body>

      <Card.Body className="text-center">

      
    {
        isActive
        ?
        <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
        :
        <Button variant="danger" type="submit" id="submitBtn">Submit</Button>

    }

    <Button as={Link} to={'/products/edit' } className="m-3" variant="info">Back</Button>
         
     </Card.Body>
    </Form>

    </Card>
    </>
    )
}