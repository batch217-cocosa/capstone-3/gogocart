import { useState, useEffect, useContext } from "react";

import {Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function ProductsView(){

	const { user } = useContext(UserContext);

	const { productId } = useParams();
	const [qty, setQty]= useState();

	const navigate = useNavigate();
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [imagesrc, setImg] = useState('');

	useEffect(()=>{
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImg(data.imagesrc)

		});

	}, [productId])

	const buy = (productId) =>{


		fetch(`${ process.env.REACT_APP_API_URL }/orders/`,{
			method: "GET",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(data => {

			if (data){

				fetch(`${ process.env.REACT_APP_API_URL }/orders/addtocart`,{
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${ localStorage.getItem('token') }`
					},
					body: JSON.stringify({
						quantity: Number(qty),
					 	productId: productId
					 
					})
				})
				.then( data => {


					if(data){
						Swal.fire({
							title: "Successfully Added",
							icon: "success",
							text: "Check your cart"
						});

						navigate("/products");
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						});
					}

				});

			}
			

			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});
			}
		})
	}


	return(
		<Form className="container-fluid" onSubmit={(e)=> buy(e)}>
		<Card className="m-5">
			<Row>
				<Col >

				<Card.Img className="pb-2"  src={imagesrc}></Card.Img>
				</Col >
				<Col >

					
						<Card.Body className="">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							</Card.Body>
							<Card.Footer>
							
							<Form.Group className="container-fluid">
								<Form.Label>Quantity:</Form.Label>
								<Form.Control
								type="number"
								placeholder="1"

								defaultValue={1}
								onChange={e => setQty(e.target.value)}
								/>
							</Form.Group>
							{
								(user.isAdmin)
								?
								<Button className="m-2" variant="danger" size="lg" disabled >Add to Cart</Button>
								:
								<Button className="m-2" variant="primary"  size="lg" onClick={() => buy(productId)}>Add to Cart</Button>
							}

							<Button as={Link} to="/products" >Go Back</Button>

							</Card.Footer>
							
								
					
				</Col>
			</Row>
		</Card>
		</Form>
	)
}