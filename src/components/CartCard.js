import { Card, Button, Row, Col} from 'react-bootstrap';
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function CartItems({CardProp}) {

	const { productId, quantity, subtotal } = CardProp;

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [pprice, setPPrice] = useState('');
    const [imagesrc, setSource] = useState('');


    useEffect(()=>{

        fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
        .then(res => res.json())
        .then(data => {


            setName(data.name);
            setPPrice(data.price);
            setDescription(data.description);
            setSource(data.imagesrc);

        });

    }, [productId])





    return (
        
          <Card className="my-3 pb-3 container-fluid">
  
            <Row>
            <Col className="my-2 col-sm-12 col-md-4 ">
                <Card.Img variant="top" src={imagesrc} style={{height:'7rem', width:'7rem'}}></Card.Img>
            </Col>
            <Col className="mx-2 col-md-4 ">
            <Card.Body>
                <Card.Title className="my-1">
                    {name}
                </Card.Title>
                <Card.Text className="my-1">
                    Price: {pprice}
                </Card.Text>
                <Card.Text className="my-1">
                    Description: {description}
                </Card.Text>
                
                
            </Card.Body>
            </Col>
            </Row>
             <Row className="my-2">
             <Col>
                <Card.Text className="m-3 position-absolute bottom-0 end-0">
                   Qty: {quantity} Subtotal: Php {subtotal}
                </Card.Text>
                </Col>
            </Row>
            
                <Button variant="danger" className="position-absolute bottom-0 start-0 m-2" disabled>Edit</Button>
            
            </Card>
        
        
    )
}