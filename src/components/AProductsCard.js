import { useContext, useState, useEffect } from "react"
import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import TextTruncate from 'react-text-truncate';
export default function AProductCard({prop}) {

	const { _id, name, description, price, inventory, imagesrc, isActive, quantity } = prop;

    const [archiveProduct, setArchive] = useState(isActive)

    var TextTruncate = require('react-text-truncate');

    const archive = (_id) => {
           fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`,
        {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {

            if(data){
                Swal.fire({
                    title: "PRODUCT ACHIVING SUCCESS!",
                    icon: "success",
                    text: `The product is now in archive`
                }).then((res) => {
                if(res.isConfirmed){
                window.location.reload();
            }
                })
                setArchive(true)
            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })

        

    }
    const unarchive = (_id) => {

        fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/unarchive`,
        {
            method : "PATCH",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {

            if(data){
                Swal.fire({
                    title: "PRODUCT ACTIVATION SUCCESS!",
                    icon: "success",
                    text: `The product is now activated`
                }).then((res) => {
                if(res.isConfirmed){
                window.location.reload();
            }
                })
                setArchive(false)
            }else{
                Swal.fire({
                    title: "Activation Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })


        
    }

    return (



        <Card className="my-3 pb-3 container-fluid" style={{ width: '17rem', height: '37rem'}}>
  
            <Card.Body>
                <Card.Img className="pb-2" variant="top" src={imagesrc}></Card.Img>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    <TextTruncate
                    line={2}
                    element="span"
                    truncateText="…"
                    text={description}
                    />
                    
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Inventory:
                </Card.Subtitle>
                
                {
                    (quantity > 0)
                    ?
                    <Card.Text>{quantity} available </Card.Text>
                    :
                    <Card.Text>Out of Stock </Card.Text>
                }
                
            </Card.Body>
            <Card.Footer className="p-1">

            <Button as={Link} to={`/edit/products/${_id}`} variant="primary" className="m-1">Edit</Button>
            {(archiveProduct)
            ?
            <Button variant="danger" className="m-1" onClick={() => archive(_id)} >Archive</Button>
            :
            <Button variant="success" className="m-1" onClick={() => unarchive(_id)} >Unarchive</Button>
            }
            </Card.Footer>
        </Card>

    )
}