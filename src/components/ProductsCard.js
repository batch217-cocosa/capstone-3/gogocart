import { Card, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import TextTruncate from 'react-text-truncate';
import { useContext } from "react"
import UserContext from "../UserContext";

export default function ProductCard({productProp}) {
    var TextTruncate = require('react-text-truncate');

    const { user } = useContext(UserContext);

	const { _id, name, description, price, inventory, imagesrc, quantity } = productProp;

    return (
          <Card className="my-3 pb-3 container-fluid" style={{ width: '17rem', height: '37rem'}}>
  
  
            <Card.Body>
                <Card.Img className="pb-2" variant="top" src={imagesrc}></Card.Img>
                <Card.Title>
                    {name}
                </Card.Title>
                <Card.Subtitle>
                    Description:
                </Card.Subtitle>
                <Card.Text>
                    <TextTruncate
                    line={2}
                    element="span"
                    truncateText="…"
                    text={description}
                    />
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Subtitle>
                    Inventory:
                </Card.Subtitle>
                <Card.Text>
                    {quantity} available
                </Card.Text>
            </Card.Body>
            {
                (user.id != null)
                ?
                <Button as={Link} to={`/products/${_id}`} variant="primary" className="position-absolute bottom-0 m-3">Details</Button>
                :
                <Button as={Link} to={`/products/${_id}`} variant="primary" className="position-absolute bottom-0 m-3 disabled">Details</Button>

            }
        </Card>
        
    )
}